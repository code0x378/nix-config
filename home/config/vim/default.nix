{ config, ... }:

{
  home.file.".vimrc".source = ./_vimrc;
}
