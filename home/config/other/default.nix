{ config, ... }:

{
  home.file.".ideavimrc".source = ./_ideavimrc;
  home.file.".pinerc".source = ./_pinerc;
  home.file.".xbindkeysrc".source = ./_xbindkeysrc;
  home.file."ignorelist".source = ./_ignorelist;
}
