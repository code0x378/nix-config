{ config, pkgs, ... }:

let

  #   where_is_my_sddm_theme_custom = pkgs.where-is-my-sddm-theme.override {
  #     themeConfig.General = {
  #         background = toString /home/jeff/data/personal/wallpaper/colors/red-gray.PNG;
  #         backgroundMode = "fill";
  #     };
  # };

  background-package = pkgs.stdenvNoCC.mkDerivation {
    name = "background-image";
    src = /home/jeff/data/personal/wallpaper/colors;
    dontUnpack = true;
    installPhase = ''
      cp $src/green-gray.PNG $out
    '';
  };

in {
  environment = {
    systemPackages = with pkgs;
      [
        (pkgs.writeTextDir "share/sddm/themes/breeze/theme.conf.user" ''
          [General]
          background = ${background-package}
        '')
      ];
  };
}
