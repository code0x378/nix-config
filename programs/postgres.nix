{ config, pkgs, ... }:

{
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_14;
    enableTCPIP = true;
    authentication = pkgs.lib.mkOverride 10 ''
      local all postgres    peer
      local all all    md5
      host all all 127.0.0.1/32   md5
      host all all ::1/128   md5
    '';
    initialScript = pkgs.writeText "backend-initScript" ''
       alter user postgres with password 'Smurffdb010';
       alter user synergy  with password '123456';
       alter user red88  with password '123456';
       alter user africa  with password '123456';
       alter user seventhings  with password '123456';
       alter user hexkit  with password '123456';
       alter user prism  with password '123456';
       alter user rcdb  with password '123456';
       alter user redbrick  with password '123456';
       alter user simplethings  with password '123456';
       alter user staticca  with password '123456';
       alter user statuso  with password '123456';
       alter user trackertine  with password '123456';
       alter user ember  with password '123456';
       alter user fusion  with password '123456';
       alter user bcm  with password '123456';
       alter user iq with password '123456';
       alter user continuva with password '123456';
    '';
    ensureDatabases = [
      "synergy"
      "red88"
      "africa"
      "bcm"
      "seventhings"
      "hexkit"
      "prism"
      "rcdb"
      "redbrick"
      "simplethings"
      "staticca"
      "statuso"
      "trackertine"
      "codexiny"
      "fusion"
      "ember"
      "trackertine_adonis"
      "genomefusion"
      "genomius"
      "cntrack"
      "staging"
      "site1"
      "site2"
      "continuva"
    ];
    ensureUsers = [
      {
        name = "synergy";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "red88";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "africa";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "bcm";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "seventhings";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "hexkit";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "prism";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "rcdb";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "redbrick";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "simplethings";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "staticca";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "statuso";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "trackertine";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "ember";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "fusion";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "bcm";
        ensureDBOwnership = true;
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "iq";
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
      {
        name = "continuva";
        ensureClauses = {
          superuser = true;
          createrole = true;
          createdb = true;
        };
      }
    ];
  };
}
