{ pkgs ? import <nixpkgs> { system = builtins.currentSystem; } }:

with pkgs;
mkShell {
  name = "flakeEnv";
  buildInputs = [ nil ];
  shellHook = ''
    alias nrb="sudo nixos-rebuild build --flake . --impure"
    alias nrt="sudo nixos-rebuild test --flake . --impure"
    alias nrs="sudo nixos-rebuild switch --flake . --impure"

    alias build-alcyone="nixos-rebuild switch --flake .#alcyone --target-host 192.168.10.46 --use-remote-sudo"
    alias build-maia="nixos-rebuild switch --flake .#maia --target-host 192.168.10.11  --use-remote-sudo"
    alias build-atlas="nixos-rebuild switch --flake .#atlas --target-host 192.168.10.20 --use-remote-sudo"

    alias reformat="find -name '*.nix' -print0 | xargs -0 nixfmt"
  '';
}
