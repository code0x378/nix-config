{ inputs, lib, config, pkgs, ... }: {

  networking = {
    nameservers = [ "9.9.9.9" "1.1.1.1" ];
    extraHosts = ''
      192.168.10.1 ss1
      192.168.10.2 ss2
 
      ################################################################
      # SYNERGY
      ################################################################

      127.0.0.1 mitotechpharma.test
      ::1 mitotechpharma.test
      127.0.0.1 mitotechpharma.site.staging.dotsynergy.test
      ::1 mitotechpharma.site.staging.dotsynergy.test

      127.0.0.1 sk-q.test
      ::1 sk-q.test
      127.0.0.1 sk-q.site.staging.dotsynergy.test
      ::1 sk-q.site.staging.dotsynergy.test

      127.0.0.1 thoughtlogix.test
      ::1 thoughtlogix.test
      127.0.0.1 thoughtlogix.site.staging.dotsynergy.test
      ::1 thoughtlogix.site.staging.dotsynergy.test

      127.0.0.1 syntiva.test
      ::1 syntiva.test
      127.0.0.1 syntiva.site.staging.dotsynergy.test
      ::1 syntiva.site.staging.dotsynergy.test

      127.0.0.1 dotsynergy.test
      ::1 dotsynergy.test
      127.0.0.1 dotsynergy.site.staging.dotsynergy.test
      ::1 dotsynergy.site.staging.dotsynergy.test

      127.0.0.1 codexiny.test
      ::1 codexiny.test
      127.0.0.1 codexiny.site.staging.dotsynergy.test
      ::1 codexiny.site.staging.dotsynergy.test

      127.0.0.1 littledogbiguniverse.test
      ::1 littledogbiguniverse.test
      127.0.0.1 littledogbiguniverse.site.staging.dotsynergy.test
      ::1 littledogbiguniverse.site.staging.dotsynergy.test

      127.0.0.1 fiddlefaddle.test
      ::1 fiddlefaddle.test
      127.0.0.1 fiddlefaddle.site.staging.dotsynergy.test
      ::1 fiddlefaddle.site.staging.dotsynergy.test

      127.0.0.1 stellachow.test
      ::1 stellachow.test
      127.0.0.1 stellachow.site.staging.dotsynergy.test
      ::1 stellachow.site.staging.dotsynergy.test

      127.0.0.1 sycamoreafrica.test
      ::1 sycamoreafrica.test
      127.0.0.1 sycamoreafrica.site.staging.dotsynergy.test
      ::1 sycamoreafrica.site.staging.dotsynergy.test

      127.0.0.1 jeffsmithdev.test
      ::1 jeffsmithdev.test
      127.0.0.1 jeffsmithdev.site.staging.dotsynergy.test
      ::1 jeffsmithdev.site.staging.dotsynergy.test

      127.0.0.1 thejeffsmith.test
      ::1 thejeffsmith.test
      127.0.0.1 thejeffsmith.site.staging.dotsynergy.test
      ::1 thejeffsmith.site.staging.dotsynergy.test

      127.0.0.1 demo.test
      ::1 demo.test
      127.0.0.1 demo.site.staging.dotsynergy.test
      ::1 demo.site.staging.dotsynergy.test

      127.0.0.1 infoquant.test
      ::1 infoquant.test
      127.0.0.1 infoquant.site.staging.dotsynergy.test
      ::1 infoquant.site.staging.dotsynergy.test

      127.0.0.1 genomius.test
      ::1 genomius.test
      127.0.0.1 genomius.site.staging.dotsynergy.test
      ::1 genomius.site.staging.dotsynergy.test

      127.0.0.1 0x378.test
      ::1 0x378.test
      127.0.0.1 0x378.site.staging.dotsynergy.test
      ::1 0x378.site.dotsynergy.test

      127.0.0.1 code0x378.test
      ::1 code0x378.test
      127.0.0.1 code0x378.site.staging.dotsynergy.test
      ::1 code0x378.site.dotsynergy.test

      127.0.0.1 red88eyewear.test
      ::1 red88eyewear.test
      127.0.0.1 red88eyewear.site.okularus.test
      ::1 red88eyewear.site.okularus.test

      127.0.0.1 okularus.test
      ::1 okularus.test
      127.0.0.1 okularus.site.staging.dotsynergy.test
      ::1 okularus.site.dotsynergy.test

      127.0.0.1 okularus_demo.test
      ::1 okularus_demo.test
      127.0.0.1 okularus_demo.site.staging.dotsynergy.test
      ::1 okularus_demo.site.dotsynergy.test

      127.0.0.1 arfarfbebe.test
      ::1 arfarfbebe.test
      127.0.0.1 arfarfbebe.site.staging.dotsynergy.test
      ::1 arfarfbebe.site.dotsynergy.test

      127.0.0.1 arfarfboutique.test
      ::1 arfarfboutique.test
      127.0.0.1 arfarfboutique.site.staging.dotsynergy.test
      ::1 arfarfboutique.site.dotsynergy.test

      127.0.0.1 ruffruffcouture.test
      ::1 ruffruffcouture.test
      127.0.0.1 ruffruffcouture.site.staging.dotsynergy.test
      ::1 ruffruffcouture.site.dotsynergy.test

      ################################################################
      # CNTRACK
      ################################################################

      127.0.0.1 cntrack.test
      ::1 cntrack.test

      127.0.0.1 genomefusion.test
      ::1 genomefusion.test

      127.0.0.1 staging.cntrack.test
      ::1 staging.cntrack.test

      127.0.0.1 site1.staging.cntrack.test
      ::1 site1.staging.cntrack.test

      127.0.0.1 site2.staging.cntrack.test
      ::1 site2.staging.cntrack.test

      ################################################################
      # LEGACY
      ################################################################

      127.0.0.1 pathfinder.test
      127.0.0.1 slevio.test

      127.0.0.1 deals.arfarfbebe.test
      127.0.0.1 designer.arfarfbebe.test
      127.0.0.1 tracker.arfarfbebe.test
      127.0.0.1 exchange.arfarfdesigner.test
      127.0.0.1 arfarfdeals.test
      127.0.0.1 arfarfexchange.test

      127.0.0.1 thoughtlogix.test
      127.0.0.1 public.thoughtlogix.test
      127.0.0.1 extranet.thoughtlogix.test
      127.0.0.1 tl.thoughtlogix.test
      127.0.0.1 intranet.thoughtlogix.test
      127.0.0.1 app.thoughtlogix.test
      127.0.0.1 downloads.thoughtlogix.test
      127.0.0.1 www.thoughtlogix.test
      127.0.0.1 menu.thoughtlogix.test
      127.0.0.1 sandbox.thoughtlogix.test
      127.0.0.1 sampledata.thoughtlogix.test
      127.0.0.1 services.thoughtlogix.test
      127.0.0.1 appweb.thoughtlogix.test

    '';
  };

}
