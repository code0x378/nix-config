# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    {
      device = "/dev/disk/by-uuid/34265415-b560-4a4d-b8b2-a88e8c3ca6b4";
      fsType = "ext4";
    };

  boot.initrd.luks.devices."luks-8d9765de-cced-4418-a977-81dd13fafe55".device = "/dev/disk/by-uuid/8d9765de-cced-4418-a977-81dd13fafe55";

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/D28C-5F3B";
      fsType = "vfat";
      options = [ "fmask=0022" "dmask=0022" ];
    };

  fileSystems."/home" =
    {
      device = "/dev/disk/by-uuid/d3979dde-7aba-4d33-82d5-ad60fc52b33f";
      fsType = "ext4";
    };

  boot.initrd.luks.devices."luks-c75d00b4-1564-4d21-916e-76dce8437d21".device = "/dev/disk/by-uuid/c75d00b4-1564-4d21-916e-76dce8437d21";

  fileSystems."/mnt/games" =
    {
      device = "/dev/disk/by-uuid/691f2194-bec7-454f-aad6-c0115bd1e473";
      fsType = "ext4";
    };

  boot.initrd.luks.devices."luks-e790e0c5-f18d-4e9e-b960-09d26d432c65".device = "/dev/disk/by-uuid/e790e0c5-f18d-4e9e-b960-09d26d432c65";

  swapDevices = [{
    device = "/swapfile";
    size = 16 * 1024; # 16GB
  }];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.eno1.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp11s0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
