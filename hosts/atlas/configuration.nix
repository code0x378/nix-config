{ inputs, lib, config, pkgs, ... }: {
  imports = [
    ../common.nix
    # ../../home/default.nix
    ../../programs/core.nix
    ../../programs/desktop.nix
    ../../programs/gaming.nix
    ../../programs/code.nix
    ../../programs/server.nix
    ../../programs/postgres.nix
    ../../programs/vmware.nix
    ../../programs/sddm.nix
    ./hardware-configuration.nix
  ];

  # Bootloader.
  #   boot.loader.grub.enable = true;
  #   boot.loader.grub.device = "/dev/sda";
  #   boot.loader.grub.useOSProber = true;
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # boot.initrd.kernelModules = [ "amdgpu" ];

  environment = {
    systemPackages = with pkgs; [ calibre apcupsd duplicacy samba4Full ];
  };

  networking = {
    hostName = "atlas";
    # interfaces = {
    #   ens33.ipv4.addresses = [{
    #     address = "192.168.10.20";
    #     prefixLength = 32;
    #   }];
    # };
    # defaultGateway = {
    #   address = "192.168.10.254";
    #   interface = "ens33";
    # };
  };

  services.samba = {
    enable = true;
    securityType = "user";
    openFirewall = true;
    extraConfig = ''
      workgroup = WORKGROUP
      server string = maia
      netbios name = maia
      security = user
      # note: localhost is the ipv6 localhost ::1
      hosts allow = 192.168.10. 127.0.0.1 localhost
      hosts deny = 0.0.0.0/0
      guest account = nobody
      map to guest = bad user
    '';
    shares = {
      data = {
        path = "/home/jeff/data";
        browseable = "yes";
        public = "no";
        writable = "yes";
        printable = "no";
        "read only" = "no";
        "guest ok" = "no";
        "directory mask" = "0755";
        "create mask" = "0765";
        "valid users" = "jsmith jeff";
      };
    };
  };

  services.samba-wsdd = {
    enable = true;
    openFirewall = true;
  };

  # Power Management
  # powerManagement.cpuFreqGovernor = "performance";

  # Accelerate package building
  nix.settings.max-jobs = 28;

  # X11
  # services.xserver.videoDrivers = [ "amdgpu" ];
}
